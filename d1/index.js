/*
	- use the 'require' directive to load the express module/packages
	-A 'module' is a software component or part of a program that contains one or more routines
	- this is used to get the contents of the package to be used by our application
	- it allows us to access methods and functions that will allow us to easily create a server
*/
const express = require("express");

/*
	- create an application using express
	- this creates an express application and stores this in a constant called app
	- In layman's term, app is our server
*/
const app = express()

//For our application server to run, we need a port to listen to
const port = 3000;

//MIDDLEWARES
/*
	- setup for allowing the server to handle data from requests
	- allows your app to read json data
	- methods used from express.js are middlewares
	- middleware is a layer of software that enables interaction and transmission of information between assorted applications. 
*/
app.use(express.json());
/*
	- allows your app to read data from forms
	- by default, information received from the URL can only be received as a string or an array
	- by applying the option of "extended:true", we are allowed to receive information in other data types such as an object throughout our application
*/
app.use(express.urlencoded({extended: true}));

//ROUTES
/*
	- express has methods corresponding each HTTP method 
	- the full base URL for our local application for this route will be at "http"//localhost:3000
*/

//RETURNS SIMPLE MESSAGE
/*
	- this route expects to receive a GET request at the base URI "/"

	POSTMAN:
	url: http://localhost:3000
	method: GET
*/
app.get('/', (request, response) => {
	response.send('Hello World')
})

//RETURNS SIMPLE MESSAGE
/*
	URI: /hello
	Method: 'GET'

	POSTMAN:
	url: http://localhost:3000/hello
	Method: GET
*/
app.get('/hello', (request, response) => {
	response.send('Hello from the "/hello" endpoint')
})

//RETURNS SIMPLE GREETING
/*
	URI: /hello
	Method: 'POST'

	POSTMAN:
	url: http://localhost:3000/hello
	Method: POST
	body: raw + json
	{
		"firstName": "Joanna",
		"lastName": "Carlos"
	}
*/
app.post('/hello', (request, response) => {
	response.send(`Hello there, ${request.body.firstName} ${request.body.lasttName}! This is from the "/hello" endpoint but with a post method`)
})

let users = []

//REGISTER USER ROUTE
/*
	- this route expects to receive a POST request at the URI "/register"
	- this will create a user object in the "users" variable that mirrors a real world registration process

	URI: /hello
	Method: 'Post'

	POSTMAN: 
	url: http://localhost:3000/register
	Method: POST
	body: raw + json
	{
		"username": "Joanna",
		"password": " "
	}
*/
app.post('/register', (request, response) => {
	
	if (request.body.username !== '' && request.body.password !== ''){
		users.push(request.body)
		response.send(`User ${request.body.username} successfully registered!`)
		console.log(request.body)
	} else {
		response.send('Please input BOTH username and password')
	}

})

//CHANGE PASSWORD
/*
	 - This route expects to receive a PUT request at the URI "/change-password"
	 - This will update the password of a user that matches the information provided in the client/postman

	 URI: /change-password
	 method: PUT

	url: http://localhost:3000/change-password
	Method: PUT
	body: raw + json
	{
		"username": "Joanna",
		"password": " "
	}
*/
app.use('/change-password', (request, response) => {
	//Creates a variable to store a message to be sent back to the client/postman
	let message;
	console.log("works after the message")

	//Creates a for loop that will loop through the elements of the "users" array 
	for (let i = 0; i < users.length; i++){
		//If the username provided in the client/postman and the username of the current object in the loop is the same
		console.log("works before the if")
		if (request.body.username == users[i].username){
			users[i].password = request.body.password
			message = `User ${request.body.username}'s password has been updated`
			break;
		} else{
			message = "User does not exist"
		}
	}
	console.log("works before the response send")
	response.send(message);
})















/*
	- tells our server which is "app" to listen to the port
	 - if the port is accessed, we can run the server
	 - Returns the message to confirm that the server is running in the terminal
*/
app.listen(port, () => console.log(`Server running at port ${port}`))
