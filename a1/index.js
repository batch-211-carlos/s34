const express = require("express");


const app = express()


const port = 3000;


app.use(express.json());

app.use(express.urlencoded({extended: true}));

/*
	POSTMAN:
	url: http://localhost:3000/signup
	method: GET
*/
let user = []

	/*URI: /signup
	Method: 'Post'

	POSTMAN: 
	url: http://localhost:3000/signup
	Method: POST
	body: raw + json
	{
		"username": "johndoe",
		"password": " "
	}*/
app.post('/signup', (request, response) => {
	
	if (request.body.username !== '' && request.body.password !== ''){
		user.push(request.body)
		response.send(`User ${request.body.username} successfully registered!`)
		console.log(request.body)
	} else {
		response.send('Please signup')
	}

})

app.get('/homepage', (request, response) => {
	response.send('Thank you for signing up')
})

let users = 
{
	"username": 'johndoe',
	"password": 'johndoe123'
}

	/*URI: /users
	Method: 'GET'

	POSTMAN:
	url: http://localhost:3000/users
	Method: GET*/

	app.get('/users', (request, response) => {
	response.send(users)
})



	/*URI: /del-users
	Method: 'DELETE'

	POSTMAN:
	url: http://localhost:3000/del-users
	Method: DELETE*/

	app.delete('/del-users', (request, response) => {
	response.send(`User johndoe has been deleted `)
})


app.listen(port, () => console.log(`Server running at port ${port}`))